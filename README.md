# Truth Social Diffs

Differences between the [Mastodon](https://github.com/mastodon/mastodon) and [Truth Social](https://codeberg.org/TruthSocial/truth-social) codebases.

The latest release in the Truth Social changelog (as of 2021-11-24) was v3.4.1. So that version has been compared with the Truth Social code. However, it appears that Truth Social forked from a slightly newer commit (PRs welcome).

## Diffs

- [Mastodon v3.4.1 to Truth Social 2021-11-24](v3.4.1-to-2021-11-24.diff) - ([raw](https://codeberg.org/TruthSocial/diffs/raw/branch/main/v3.4.1-to-2021-11-24.diff))

